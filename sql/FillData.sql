/*=======������ ���������� ������� ������ �� search_person==*/
/*====Created: Fedorov R.A. Creation Date: 19.06.2016. Last Change: 19.06.2016====*/
 /*��������� ���������� �������*/
INSERT INTO web_application.city(name)
VALUES ('������'), ('�����-���������'), ('�����������'),
 ('������������'), ('���������'), ('�����'), ('����������'),
 ('�������'), ('�������'), ('����');
 
 /*��������� ���������� �����*/
INSERT INTO web_application.car(name)
VALUES ('Toyota RAV 4'), ('Nissan Teana'), ('Lada Priora'),
 ('Lada Vesta'), ('Lada XRay'), ('Honda Civic'), ('Subaru Legacy'),
 ('Lexus RS 300'), ('Chevrolet Spark'), ('Renault Duster');

 /*��������� ������� �����*/
INSERT INTO  web_application.persons(surname, name, patronymic, city_id, car_id)
VALUES ('�������', '�����', '���������', 9, 9),
 ('������', '������', '��������', 10, NULL),
 ('��������', '�������', '����������', 1, 2),
 ('�������', '������', '�������', 4, 7),
 ('�����������', '�����', '����������', 2, 1),
 ('���������', '����', '��������������', 8, 3),
 ('James', 'David', '', 6, 5),
 ('�����', '�������', '��������', 5, 4),
 ('��������', '�������', '�����������', NULL, 8),
 ('��������', '���������', '�������������',3, 10);

 /*��������� ������� �������������*/
INSERT INTO  web_application.users(login, password, surname, name, patronymic, enabled)
VALUES ('testusr', '$2a$10$sVxS7bXsNN4AnK349sj.YevaPqKFxu5vE0ZLxZYSK9puem2MwZKaG', '������', '����', '��������', 1);

INSERT INTO  web_application.user_roles(user_id, role)
VALUES (1, 'ROLE_ADMIN'), (1, 'ROLE_USER');

 /*������������� ��� ������, ����� ���������� ������*/
 REINDEX INDEX web_application.indx_persons_surname_name_patronymic;
 
 