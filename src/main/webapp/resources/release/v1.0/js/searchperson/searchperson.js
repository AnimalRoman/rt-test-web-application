
var domain =""; //Для AJAX-запроса, если домен отличается от домена текущего приложения

//Подготавливает CSRF атрибуты и включает их в заголовок AJAX запросов   
$(function () {
var token = $("meta[name='_csrf']").attr("content");
var header = $("meta[name='_csrf_header']").attr("content");
$(document).ajaxSend(function(e, xhr, options) {
xhr.setRequestHeader(header, token);
});
});

	jQuery(document).ready(function($) {

		//Событие при подтверждении формы поиска
		$("#search-form").submit(function(event) {

			// делаем недоступными кнопки управления поиском на время выполнения поиска
			enableButtons(false);

			// Предотваращаем передачу данных из формы на сервер
			event.preventDefault();
        
             //Проверяем введен ли хотя бы один из критериев поиска
			//и если да - выполняем поиск, если нет - выводим пользователю
			//соответствующее сообщение
            
			  checkSearchCriteria();
			           
		} );
		
		//Событие при нажатии на кнопку Очистить
		$('#btn-clear').click(function ()
				{
	    	$('#surname').val(''); //Фамилия 
	    	$('#name').val('');  //Имя 
	    	$('#patronymic').val(''); //Отчество 
	    	$('#city').val(''); //Город 
	    	$('#car').val(''); //Машина 
				});
				
		$('#search-results').hide().html('Введите критерии поиска и нажмите кнопку "Найти"').fadeIn(650);
	});
        
  //Функция для проверки того был ли введен пользователем хотя бы один критерий поиска
    function checkSearchCriteria ()
    {
    	var srn = $.trim($('#surname').val()); //Фамилия без лишних пробелов
    	var nm = $.trim($('#name').val());  //Имя без лишних пробелов
    	var ptr = $.trim($('#patronymic').val()); //Отчество без лишних пробелов
    	var cty = $.trim($('#city').val()); //Город без лишних пробелов
    	var car = $.trim($('#car').val()); //Машина без лишних пробелов
    
    	if(!srn && !nm && !ptr && !cty && !car) //Если все поля пустые без лишних пробелов
    		{
    		//Выводим сообщение
        	$('#search-criteria-info').html("Пожалуйста заполните хотя бы один критерий поиска");
    		}
    	else //Заполнен хотя бы один критерий поиска
    		{
   	   //Убираем сообщение
    	$('#search-criteria-info').html("");
    	 //Выполняем поиск через Ajax
		searchPerson();
    		}   	
    }
    
    //Функция поиска людей
	function searchPerson() {

	      $('#search-results').html('Пожалуйста подождите...');   
		
		//Формирования входных данных, согласно введенным критериям поиска
		var personToSearch = {}
		personToSearch["surname"] = $("#surname").val();
		personToSearch["name"] = $("#name").val();
        personToSearch["patronymic"] = $("#patronymic").val();
        personToSearch["city"] = $("#city").val();
        personToSearch["car"] = $("#car").val();

        //Формирование и выполнение AJAX-запроса
		$.ajax({
			type : "POST",
			contentType : "application/json",
			url : domain +  "api/SearchPerson",
			data : JSON.stringify(personToSearch),
			dataType : 'json',
			timeout : 100000,
			success : function(data) {
				processData(data, true);
			},
			error : function(error) {
				processData("", false);
			},
			done : function(result) {
				enableButtons(true);
			}
		});

	}

	function enableButtons(flag) {
		$("#btn-search").prop("disabled", flag);
        $("#btn-clear").prop("disabled", flag);
	}

	//Функция обработки ответа от AJAX-запроса
	function processData(data, isSuccess) {
         
		var searchResult;
		
        if(isSuccess == true) //AJAX-запрос выполнился успешно
        {                   
           if(data['code'] == '200') //AJAX-запрос вернул правильные данные
           {   
        
        	   //Формируем таблицу, в которую будем размещать полученные данные
        var htmltable = "<table class=\"search__table\">" + 
                        "<thead>" +
                        "<tr>"+
                        "<th>ФИО</th>"+
                        "<th>Город проживания</th>"+
                         "<th>Машина</th>"+
                         "</tr>"+
                         "</thead>";  
        
        //Формируем шаблон для плагина json2html - для мэппинга данных из ответа AJAX в формат HTML
       var transformPattern = 
    {"tag":"tbody","children":[
        {"tag":"tr","children":[
            {"tag":"td","html":"${surname} ${name} ${patronymic}"},
              {"tag":"td","html":"${city}"},
               {"tag":"td","html":"${car}"}
        ]}
    ]};
       //Выполняем мэппинг данных AJAX->HTML, собираем результирующую таблицу 
       searchResult = htmltable.concat((json2html.transform(JSON.stringify(data['result']),transformPattern))).concat("</table>");                     
           }
            else if (data['code'] == '204') //AJAX-запрос вернул пустые данные
            {
             searchResult =  "<label>По Вашему запросу ничего не найдено.<br/>Попробуйте добавить или уточнить критерии поиска.</label>";
            }
        else if (data['code'] == '400' ) //AJAX-запрос вернул ошибку о неправильном запросе или о недостаточных входных данных
        {
        	   searchResult =  "<label>Пожалуйста добавьте или уточните критерии поиска и выполните поиск заново.</label>";
        }
        else //AJAX-запрос вернул сообщение о том что на сервере произошла ошибка при выполнении метода "поиска людей"
        {
        searchResult =  "<label>Произошла ошибка.<br/>Попробуйте выполнить поиск немного позже.</label>";
        }                    
        }
        else //AJAX-запрос не выполнился или выполнился с ошибкой
        {
         searchResult =  "<label>Произошла ошибка.<br/>Попробуйте выполнить поиск немного позже.</label>";
        }      
      //Вставляем результаты на страницу  
        $('#search-results').hide().html(searchResult).fadeIn(650); 
	}
