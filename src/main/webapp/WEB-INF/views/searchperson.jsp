<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html>
<html>
<head>
<title>Поиск людей</title>

<spring:url value="/resources/release/v1.0/css/compiled.css" var="generalCSS" />	
<spring:url value="/resources/release/v1.0/js/core/jquery-1.12.4.min.js" var="jQueryJS" />
<spring:url value="/resources/release/v1.0/js/core/json2html.js" var="json2htmlcoreJS" />
<spring:url value="/resources/release/v1.0/js/core/jquery.json2html.js" var="json2htmljqueryJS" />
<spring:url value="/resources/release/v1.0/js/searchperson/searchperson.js" var="pageJS" />

<link href="${generalCSS}" rel="stylesheet" />
<script src="${jQueryJS}"></script>	
<script src="${json2htmlcoreJS}"></script>	
<script src="${json2htmljqueryJS}"></script>	
<script src="${pageJS}"></script>	

<sec:csrfMetaTags />
</head>

<body>
<div class="global-wrapper">
<jsp:include page="pageparts/header.jsp"/>
<jsp:include page="pageparts/operationalpanel.jsp"/>

<div class="main-content">

<div class="search">

<div class="search__left-panel">

<div class="search__search-form">
<div id="search-criteria-info" class="search__criteria-info" >
</div>
<form id="search-form" >
<div class="search__div">
<label class="search__label">Фамилия: </label>
<input class="search__input" type=text id="surname" maxlength=100/>
</div>
<div class="search__div">
<label class="search__label">Имя: </label>
<input class="search__input" type=text id="name" maxlength=100/>
</div>
<div class="search__div">
<label class="search__label">Отчество: </label>
<input class="search__input" type=text id="patronymic" maxlength=100/>
</div>
<div class="search__div">
<label class="search__label">Город: </label>
<input class="search__input" type=text id="city" maxlength=50/>
</div>
<div class="search__div">
<label class="search__label">Машина: </label>
<input class="search__input" type=text id="car" maxlength=100/>
</div>
<div class="search__div">
<button class="search__btn" type=submit id="btn-search">Найти</button>
<button class="search__btn" type="button" id="btn-clear">Очистить</button>
</div>
 <sec:csrfInput />			
</form>
</div>
</div>

<div class="search__right-panel">
<div class="search__results" id="search-results"></div>
</div>
</div>
</div>
<jsp:include page="pageparts/footer.jsp"/>
</div>
</body>
</html>