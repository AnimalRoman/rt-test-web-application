<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<div class="operational-panel">
<div class="operational-panel__content-box">
	<!-- Эта секция доступна только для тех пользователей, у которых есть роль - USER -->
<sec:authorize access="hasRole('ROLE_USER')">
	
		<c:url value="/j_spring_security_logout" var="logoutUrl" />
		<form action="${logoutUrl}" method="post" id="logoutForm">
	  <sec:csrfInput />
		</form>
		
		<script>
			function formSubmit() {
				document.getElementById("logoutForm").submit();
			}
		</script>

		<c:if test="${pageContext.request.userPrincipal.name != null}">
			<div class="operational-panel__user-info">
				<label class="operational-panel__user-name"> ${pageContext.request.userPrincipal.name} </label>  <a class="operational-panel__logout-btn"
					href="javascript:formSubmit()">Выйти</a>
			</div>
		</c:if>

	</sec:authorize>
	</div>
</div>