<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="true" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<title>Добро пожаловать! Поиск людей</title>
	<spring:url value="/resources/release/v1.0/css/compiled.css" var="generalCSS" />	
	<link href="${generalCSS}" rel="stylesheet" />	
</head>

<body onload='document.loginForm.login.focus();'>

<div class="global-wrapper">
<jsp:include page="pageparts/header.jsp"/>

<div class="main-content">

<div class="login-form">
<div class="login-form__content">

		<div class="login-form__welcome">Вход в сервис</div>

		<c:if test="${not empty msg}">
			<div class="login-form__failure-message">${msg}</div>
		</c:if>

       <c:url var ="loginUrl" value="/j_spring_security_check"/>

		<form name='loginForm' action="${loginUrl}" method='POST'>

		<table>
			<tr>
				<td>Логин:</td>
				<td><input type='text' name='login'></td>
			</tr>
			<tr>
				<td>Пароль:</td>
				<td><input type='password' name='password' /></td>
			</tr>
			<tr>
				<td colspan='2'><input class="login-form__submit" name="submit" type="submit"
				  value="Войти" /></td>
			</tr>
		  </table>

  <sec:csrfInput />

		</form>
		</div>
		</div>
		</div>

<jsp:include page="pageparts/footer.jsp"/>
</div>
</body>
</html>