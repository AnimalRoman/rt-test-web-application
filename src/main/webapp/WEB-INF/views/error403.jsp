<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="true" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<title>Доступ запрещен</title>
	<spring:url value="/resources/release/v1.0/css/compiled.css" var="generalCSS" />	
	<link href="${generalCSS}" rel="stylesheet" />	
</head>
<body>
<div class="global-wrapper">
<jsp:include page="pageparts/header.jsp"/>
<div class="main-content">
<div class="error-403">
<div class="error-403__content">В доступе отказано!</div>
</div>
</div>
<jsp:include page="pageparts/footer.jsp"/>
</div>
</body>
</html>