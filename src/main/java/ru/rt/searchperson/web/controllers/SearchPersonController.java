package ru.rt.searchperson.web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *  ���������� ��� ��������� ������� �� ������ � �������� ������ �����
 * @author Roman
 *
 */

@Controller
public class SearchPersonController
{
	@RequestMapping(value = {"/search", "/Search"}, method = RequestMethod.GET)
	public String ShowSearchPersonView(Model model)
	{	
		return "searchperson";
	}
	
}
