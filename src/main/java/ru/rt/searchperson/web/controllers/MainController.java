package ru.rt.searchperson.web.controllers;


import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * ������� ���������� - ������������ �������� ������� ����������, �� ��������� � ������ �������
 * @author Roman
 *
 */


@Controller
public class MainController {
	 
	
	@RequestMapping(value = {"/"}, method = RequestMethod.GET)
	public String RedirectToSearchPerson()
	{
		return "redirect:/Search";
	}
	
	@RequestMapping(value = {"/login", "/Login"}, method = RequestMethod.GET)
	public String showLogin(Model model, @RequestParam(value = "failure", required = false) String failureToLogin)
	{	
		if(failureToLogin != null)
		{
			model.addAttribute("msg", "�������� ����� ��� ������!" );	
		}

		  //��������� ��������� �� ��� ������������ ��� ���. 
		  //���� ��, �� �������������� �� ������� �������� ����������
		  //����� ���������� ����� ������
		Authentication auth; 
		try
		{
		 auth = SecurityContextHolder.getContext().getAuthentication();
		}
		catch (Exception e){auth = null;}
		  if (!(auth instanceof AnonymousAuthenticationToken)) {
			  return "redirect:/Search";
		  }
			
		return "login";
	}
	
	
	@RequestMapping(value = "/AccessDenied", method = RequestMethod.GET)
	public String showError403(Model model)
	{			
		return "error403";
	}
		
}
