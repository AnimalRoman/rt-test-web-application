package ru.rt.searchperson.web.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import ru.rt.searchperson.dao.impl.JDBCPersonDAO;
import ru.rt.searchperson.model.AjaxResponse;
import ru.rt.searchperson.model.Person;
import ru.rt.searchperson.web.jsonview.Views;

/**
 * ���������� API - ������������ ������ �� ����� ��������
 * @author Roman
 *
 */

//@RequestBody - ������������ ������ Json � ������ ���������� ����
@RestController
public class APIController
{	
	
	@Autowired
	JDBCPersonDAO jdbcPersonDAO;
	
	/*��� ��� � ������ ���� � ������� �������� ������ (���, �����, ������) � ���� ������� � �� (person)
	*���������, � �� ���� ��������� ��������� ����� ��� �������� ������, �� ����� ��������������� 
	*������������ ������� Person (����� �� ��������� ������������� ���) 
	*/	
	@JsonView(Views.Public.class) 
	@RequestMapping(value = "/api/SearchPerson", method = RequestMethod.POST)
	public AjaxResponse<Person> SearchPerson(@RequestBody Person personToSearch)
	{
		AjaxResponse<Person> result = new AjaxResponse<Person>();
		
		try
		{
		if(ValidateSearchCriteria(personToSearch))
		{
			List<Person> persons = jdbcPersonDAO.SearchPerson(personToSearch.getSurname(),
															  personToSearch.getName(),
															  personToSearch.getPatronymic(), 
															  personToSearch.getCity(), 
															  personToSearch.getCar());
		
			if(persons.size()>0)
			{
				result.setCode("200");	//Ok
				result.setMsg("");
				result.setResult(persons);
			}
			else
			{
				result.setCode("204");
				result.setMsg("No persons found. Please try to change your search criteria.");
			}
					
		}
		else
		{
			result.setCode("400");
			result.setMsg("Wrong or incomplete search criteria.");
		}
		}
		catch(Exception e)
		{
			result.setCode("500");
			result.setMsg("Something went wrong. Please try again later.");	
		}
		
		//��������� ����� ������������� � JSON
		//� ��������� � �������� ������ �� ������ (��������� @RestController)
		return result; 
		
	}
	
	/**
	 * ����� ��� �������� �������� ������ �� ������ �������
	 * @param searchCriteria - �������� ������ (� ������ ������ ������� - Person)
	 * @return true - ���� �������� ������ ����������� ���������,
	 *  false - �������� ������ ����������� ����������� ��� �����������
	 */
	private boolean ValidateSearchCriteria(Person searchCriteria)
	{		
		//��������� ��� �� ������� �������� ������, 
		//� ����� ��������� �� ���� �� ���� ���� �������� ������		
        boolean validationResult = 
    		    (searchCriteria == null || 
				(StringUtils.isEmpty(StringUtils.trimAllWhitespace(searchCriteria.getSurname())) &&
				StringUtils.isEmpty(StringUtils.trimAllWhitespace(searchCriteria.getName())) && 
				StringUtils.isEmpty(StringUtils.trimAllWhitespace(searchCriteria.getPatronymic())) && 
				StringUtils.isEmpty(StringUtils.trimAllWhitespace(searchCriteria.getCity())) &&
				StringUtils.isEmpty(StringUtils.trimAllWhitespace(searchCriteria.getCar()))))? false : true;
		
		 return validationResult;	
	}
	
}
