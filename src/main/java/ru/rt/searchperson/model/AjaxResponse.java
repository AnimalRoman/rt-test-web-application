package ru.rt.searchperson.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonView;

import ru.rt.searchperson.web.jsonview.Views;

public class AjaxResponse <T> 
{
	@JsonView(Views.Public.class)
	String msg, code;	
	@JsonView(Views.Public.class)
	List <T> result;
	
	//region Getters And Setters
	public String getMsg()
	{
	return msg;
	}	
	public void setMsg(String msg)
	{
		this.msg = msg;
	}
	public String getCode()
	{
	return code;
	}	
	public void setCode(String code)
	{
		this.code = code;
	}
	public List<T> getResult()
	{
	return result;
	}	
	public void setResult(List<T> result)
	{
		this.result = result;
	}
	//endregion
	
	//��� ���������-����������� �����
	@Override
	public String toString() {
		return "AjaxResponse [msg=" + msg + ", code=" + code + ", result=" + result + "]";
	}
	
}
