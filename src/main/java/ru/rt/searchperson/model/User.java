package ru.rt.searchperson.model;

import com.fasterxml.jackson.annotation.JsonView;

import ru.rt.searchperson.web.jsonview.Views;

/**
 * ������������ ���-����������
 * @author Roman
 *
 */
public class User {
	
	public User() {}
		
	@JsonView(Views.Public.class)
	private String login, //�����
	surname, //�������
    name,  //���
    patronymic; //��������

	public User (String login, String surname, String name, String patronymic)
	{
		this.login = login;
		this.surname = surname;
		this.name = name;
		this.patronymic = patronymic;
	}
		
	//region Getters And Setters
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPatronymic() {
		return patronymic;
	}
	public void setPatronymic(String patronymic) {
		this.patronymic = patronymic;
	}
	//endregion
}
