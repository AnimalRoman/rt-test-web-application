package ru.rt.searchperson.model;

import com.fasterxml.jackson.annotation.JsonView;
import ru.rt.searchperson.web.jsonview.Views;

/**
*�������, ������ �������� �� ����� ����������� (������)
*@author Roman
*/
public class Person {
	
	public Person () {}
	
	@JsonView(Views.Public.class)
	private String surname,//�������
	                name,  //���
	                patronymic, //��������
	                city, //����� ����������
	                car; //����������

	public Person (String surname, String name, String patronymic, String city, String car)
	{
		this.surname = surname;
		this.name = name;
		this.patronymic = patronymic;
		this.city = city;
		this.car = car;		
	}
	
	//region Getters And Setters

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPatronymic() {
		return patronymic;
	}

	public void setPatronymic(String patronymic) {
		this.patronymic = patronymic;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCar() {
		return car;
	}

	public void setCar(String car) {
		this.car = car;
	}
	//endregion
}
