package ru.rt.searchperson.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import ru.rt.searchperson.dao.IUserDAO;
import ru.rt.searchperson.model.User;

@Repository
public class JDBCUserDAO implements IUserDAO 
{
	 private NamedParameterJdbcTemplate  npJdbcTemplate;
	 
	 @Autowired
	 public void setDataSource(DataSource dataSource) {
	        this.npJdbcTemplate= new NamedParameterJdbcTemplate(dataSource);
	    }
	
	@Override
	public List<User> AuthentificateUser(String login, String password)
	{
     String query = "SELECT * FROM web_application.users_authentificate_user(:_login, :_password)";
     SqlParameterSource params = new MapSqlParameterSource().addValue("_login", login)
    		  												.addValue("_password", password);	 
     return npJdbcTemplate.query(query, params, new UserMapper());
	}

	private static final class UserMapper implements RowMapper<User>
	{

		@Override
		public User mapRow(ResultSet resultSet, int rowNum) throws SQLException 
		{		
			User user = new User();
			user.setLogin(resultSet.getString("login"));
			user.setSurname(resultSet.getString("surname"));
			user.setName(resultSet.getString("name"));
			user.setPatronymic(resultSet.getString("patronymic"));
			return user;		
		}
		
	}
	
}
