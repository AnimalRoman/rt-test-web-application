package ru.rt.searchperson.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import ru.rt.searchperson.dao.IPersonDAO;
import ru.rt.searchperson.model.Person;

@Repository
public class JDBCPersonDAO implements IPersonDAO
{

 private NamedParameterJdbcTemplate  npJdbcTemplate;
	 
	 @Autowired
	 public void setDataSource(DataSource dataSource)
	    {
	        this.npJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	    }
	
	@Override
	public List<Person> SearchPerson(String surname, String name, String patronymic, String city, String car)
	{
		String query = "SELECT * FROM web_application.persons_find_person"
				       + "(:_surname, :_name, :_patronymic, :_city, :_car)";
	    SqlParameterSource params = new MapSqlParameterSource().addValue("_surname", surname)
	    		  												.addValue("_name", name)
	    														.addValue("_patronymic",patronymic)
	    														.addValue("_city",city)
	    														.addValue("_car", car);
	    
		return npJdbcTemplate.query(query, params, new PersonMapper());
	}

	private static final class PersonMapper implements RowMapper<Person>
	{
		@Override
		public Person mapRow(ResultSet resultSet, int rowNum) throws SQLException 
		{
			Person person = new Person();
			person.setSurname(resultSet.getString("surname"));
			person.setName(resultSet.getString("name"));
			person.setPatronymic(resultSet.getString("patronymic"));
			person.setCity(resultSet.getString("city"));
			person.setCar(resultSet.getString("car"));
			return person;
		}		
	}
	
	
}
