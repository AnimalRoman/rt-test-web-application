package ru.rt.searchperson.dao;

import java.util.List;
import ru.rt.searchperson.model.User;

public interface IUserDAO
{
	public List<User> AuthentificateUser(String login, String password);
}
