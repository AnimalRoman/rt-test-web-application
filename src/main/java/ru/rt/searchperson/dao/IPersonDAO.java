package ru.rt.searchperson.dao;

import java.util.List;
import ru.rt.searchperson.model.Person;

public interface IPersonDAO 
{
	public List<Person> SearchPerson (String surname, String name, 
			                          String patronymic, String city, String car); 
}
